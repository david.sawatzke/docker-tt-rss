# tt-rss for Docker

(c) 2016 David Sawatzke <david@sawatzke.de>

Redistribution and modifications are welcome, see the LICENSE file for details.

[tiny-tiny-rss](https://tt-rss.org/) is a multipurpose rss reader

This Dockerfile provides a selfoss image with the feediron plugin (to extract the text from sites with an incomplete rss feed) and a feedly-like theme.

## Setup

All data (currently only the postgresql database) is stored in `/data`, which is persisted as a volume, but it might be reasonable to export it to the local file system. Port 80 is exposed for the web application.

### Database

This image uses a postgresql database at `/data/database`, in which everything is stored by tt-rss.

### Environment variables

Tiny-Tiny-RSS needs the URL under which it runs for a few tasks (such as sharing your public rss feed). The url is set by the `URL` environment variable. If the environment variable isn't set, the default URL is `http://example.com/`.

## Running a Container

The most basic run command would be:

    docker run -d \
    	--name tt-rss \
    	--publish 80:80 \
	-e URL=http://example.com/tt-rss/
    	--volume $TT-RSS-DATA:/data \
    	$CONTAINERNAME

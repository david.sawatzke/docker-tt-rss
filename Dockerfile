FROM alpine:3.11
MAINTAINER David Sawatzke <david@sawatzke.de>

#Copy files
COPY supervisord.conf /etc/supervisor/
COPY php.ini /etc/php7/
COPY config.php /tt-rss/
COPY init.sh /
COPY httpd.conf /etc/
VOLUME /data

#Install neccessary packets
RUN apk add --no-cache \
      php7 \
      php7-cgi \
      php7-curl \
      php7-json \
      php7-pgsql \
      php7-iconv \
      php7-dom \
      php7-pdo \
      php7-pdo_pgsql \
      php7-xml \
      php7-mbstring \
      php7-session \
      php7-fileinfo \
      php7-gd \
      php7-intl \
      postgresql \
      ca-certificates \
      supervisor \
      busybox-extras \
      openssl \
#Install tt-rss
    && wget -O /tmp/master.tar.gz https://git.tt-rss.org/fox/tt-rss/archive/master.tar.gz \
    && tar -xzf /tmp/master.tar.gz -C /tmp \
#The /tt-rss/ directory is already created, so we only move the contents
    && mv /tmp/tt-rss/* /tt-rss/ \
    && rm -rf /tmp/* \
#Install feediron
    && wget https://github.com/m42e/ttrss_plugin-feediron/archive/master.zip \
    && unzip -q master.zip \
    && mv ttrss_plugin-feediron-master/ /tt-rss/plugins/feediron/ \
    && rm -f master.zip \
#Install the feedly theme
    && wget https://github.com/levito/tt-rss-feedly-theme/archive/master.zip \
    && unzip -q master.zip \
    && cp -r tt-rss-feedly-theme-master/feedly/ tt-rss-feedly-theme-master/feedly.css /tt-rss/themes/ \
    && rm -rf master.zip tt-rss-feedly-theme-master \
#Add user
   && adduser -SDHs /sbin/nologin www-data \
#Cronjob for updates
    && echo "*/15 * * * * php /tt-rss/update.php --feeds --quiet" | crontab -u www-data - \
#Fix permissions
    && chown -R www-data /tt-rss \
    && chmod 500 /init.sh \
#Postgres fixes (This really shouldn't be needed)
    && mkdir /run/postgresql \
    && chown postgres /run/postgresql

CMD ["/init.sh"]

#!/bin/sh

if [ ! -f /data/do-not-remove ]
then
	echo "Generating database"
	#Be sure
	rm -rf /data/database
	mkdir -p /data/database
	chown postgres /data/database
	su postgres <<-EOF
		initdb --locale $LANG -E=UTF8 -D '/data/database'
		pg_ctl -D "/data/database" \
			-o "-c listen_addresses=''" \
			-w start
		psql --username postgres <<-EOSQL
			CREATE DATABASE "tt-rss" ;
		EOSQL
		psql --username postgres <<-EOSQL
			CREATE USER "tt-rss" WITH SUPERUSER ;
		EOSQL
		psql --username tt-rss tt-rss < /tt-rss/schema/ttrss_schema_pgsql.sql
		pg_ctl -D "/data/database" -m fast -w stop
	EOF
	touch /data/do-not-remove
fi
if [ -u "$URL" ]; then
	URL=http://example.com/
fi
# We can just run this every thime, it doesn't matter
sed -i -e "s@\${URL}@$URL@g" /tt-rss/config.php
chown -R postgres /data/database
exec supervisord -c /etc/supervisor/supervisord.conf
